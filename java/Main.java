class Main {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
        int n = 20, a = 0, b = 1, temp = 0;
        while (n > 1) {
            n = n - 1;
            temp = b;
            b = b + a;
            a = temp;
            System.out.println(b);
        }

        final int first = 5;
        int second = 2;
        System.out.println(first + second*second*second);
        System.out.println(subtract(100, 20));

        n = 100;
        while (n > 1) {
            n = n - 1;
            System.out.println(fun(200));
        }
    }
    
    public static int subtract(int a, int b) {
        return a - b;
    }

    public static int fun(int k) {
        if (k > 0) {
            return k + fun(k - 1) + k * 2;
        } else {
            return 0;
        }
    }

}
