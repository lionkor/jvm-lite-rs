use strum::{EnumIter, FromRepr};

/// An AccessFlag is used in all sorts of places, like inside the JVM's constant
/// pool. An access flag tells the JVM from where a field, method, class, interface, etc.
/// is accessible. These are somewhat equivalent to the access specifiers "public", "private",
/// etc. of the Java language.
#[derive(FromRepr, EnumIter, Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u16)]
pub enum AccessFlag {
    Public = 0x0001,
    Private = 0x0002,
    Protected = 0x0004,
    Static = 0x0008,
    Final = 0x0010,
    Super = 0x0020,
    Volatile = 0x0040,
    Transient = 0x0080,
    Interface = 0x0200,
    Abstract = 0x0400,
    Synthetic = 0x1000,
    Annotation = 0x2000,
    Enum = 0x4000,
    Module = 0x8000,
}
