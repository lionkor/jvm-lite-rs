use thiserror::Error;
use crate::access_flag::AccessFlag;
use crate::attribute_info::{AttributeInfo, CodeAttribute};

/// An error from method info related code.
#[derive(Error, Debug)]
pub enum MethodInfoError {
    #[error("This method has no Code attribute, which is not allowed")]
    MethodHasNoCode
}

/// Contains all information about a method of a class. Usually these also contain a code
/// attribute, which can be retrieved via [`MethodInfo::get_code`]. The type info (descriptor) is
/// not parsed and needs to be parsed to be usable.
#[derive(Debug, Clone)]
pub struct MethodInfo {
    pub access_flags: Vec<AccessFlag>,
    pub name: String,
    pub descriptor: String,
    pub attributes: Vec<AttributeInfo>,
}

impl MethodInfo {
    /// Returns the code of this [`MethodInfo`], if the method has code, otherwise errors.
    pub fn get_code(&self) -> Result<&CodeAttribute, MethodInfoError> {
        self.attributes.iter().find_map(|attrib| if let AttributeInfo::Code(code_attrib) = attrib {
            Some(code_attrib)
        } else {
            None
        }).ok_or(MethodInfoError::MethodHasNoCode)
    }
}
