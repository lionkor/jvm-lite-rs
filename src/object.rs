use crate::vm::ResolvedReference;

/// Denotes an instance of a reference type in the JVM. This can hold a string, type info, objects,
/// the "this" pointer, or any kind of reference to methods or fields. This is used in
/// [`crate::Operand`].
#[derive(Debug, Clone)]
pub enum Ref {
    Type(String),
    String(String),
    Method(ResolvedReference),
    Field(ResolvedReference),
    Object,
    This,
}
