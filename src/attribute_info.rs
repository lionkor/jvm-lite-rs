use std::fmt::Debug;
use crate::cp_info::CpInfo;
use crate::exception_table::ExceptionTable;
use crate::stack_map_frame::StackMapFrame;

/// Attribute infos are part of the constant pool and hold different kinds of values, 
/// for example code, tables, or other information.
#[derive(Debug, Clone)]
pub enum AttributeInfo {
    Code(CodeAttribute),
    Other,
    ConstantValue(CpInfo),
    LineNumberTable(LineNumberTableAttribute),
    StackMapTable(StackMapTableAttribute),
}

/// Line numbers act like debug info.
#[derive(Debug, Clone)]
pub struct LineNumberEntry {
    pub start_pc: u16,
    pub line_number: u16,
}

/// Holds a number of line numbers, which is practically debug info.
#[derive(Debug, Clone)]
pub struct LineNumberTableAttribute {
    pub line_number_table: Vec<LineNumberEntry>,
}

/// Holds stack map frames.
#[derive(Debug, Clone)]
pub struct StackMapTableAttribute {
    pub entries: Vec<StackMapFrame>,
}

/// Holds executable byte code and information associated with it, which
/// is necessary to correctly execute the given bytecode. The code
/// is not parsed in any form and simply exists as bytes at this point.
/// The max locals and stack are ignored by this JVM.
#[derive(Debug, Clone)]
pub struct CodeAttribute {
    pub max_stack: u16,
    pub max_locals: u16,
    pub code: Vec<u8>,
    pub exception_tables: Vec<ExceptionTable>,
    pub attributes: Vec<AttributeInfo>,
}
