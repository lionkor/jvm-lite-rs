use crate::cp_info::CpInfo;
use thiserror::Error;

/// Can occurr when the constant pool does not contain an item that is expected.
#[derive(Error, Debug)]
pub enum ConstantPoolError {
    #[error("Couldn't get constant pool index as string: index {0}, ({1:#?})")]
    FailedGetString(usize, Option<CpInfo>),
    #[error("Couldn't get constant pool index {0}")]
    FailedGet(usize),
}

/// The constant pool is a tree-like structure which is parsed into this vector of constant pool
/// info objects. The tree-like structure continues to exist within the `CpInfo` structs contained
/// within.
#[derive(Debug)]
pub struct ConstantPool {
    pub entries: Vec<CpInfo>,
}

impl ConstantPool {
    /// Creates a new [`ConstantPool`].
    pub fn new(cap: usize) -> Self {
        ConstantPool {
            entries: Vec::with_capacity(cap),
        }
    }

    /// Returns the [`CpInfo`] at the given index.
    pub fn get(&self, index: usize) -> anyhow::Result<&CpInfo> {
        self.entries
            .get(index - 1)
            .ok_or(ConstantPoolError::FailedGet(index - 1).into())
    }

    /// Returns the [`CpInfo`] at the given index as string. If it's convertable to a string in any
    /// way, the conversion will take place, otherwise it will error.
    pub fn get_string(&self, index: usize) -> anyhow::Result<&String> {
        match self.entries.get(index - 1) {
            None => Err(ConstantPoolError::FailedGetString(index - 1, None).into()),
            Some(val) => match val {
                CpInfo::ClassInfo { name_index } => self.get_string(*name_index as usize),
                CpInfo::StringInfo { string_index } => self.get_string(*string_index as usize),
                CpInfo::Utf8Info(str) => Ok(str),
                _ => {
                    Err(ConstantPoolError::FailedGetString(index - 1, Some((*val).clone())).into())
                }
            },
        }
    }
}
