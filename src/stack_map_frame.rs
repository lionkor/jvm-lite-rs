use strum::FromRepr;

/// A verification type specifies the type of either one or two locations, where a location is
/// either a single local variable or a single operand stack entry.
#[derive(Debug, Clone, FromRepr)]
#[repr(u8)]
pub enum VerificationTypeInfo {
    Top = 0,
    Integer = 1,
    Float = 2,
    /// Warning: Next must be Top
    Double = 3,
    /// Warning: Next must be Top
    Long = 4,
    Null = 5,
    UninitializedThis = 6,
    Object { cpool_index: u16 } = 7,
    Uninitialized { offset: u16 } = 8,
}

/// A stack map frame.
#[derive(Debug, Clone)]
pub enum StackMapFrame {
    /// 0-63
    /// This frame type indicates that the frame has exactly the same local variables as the
    /// previous frame and that the operand stack is empty. The offset_delta value for the frame
    /// is the value of the tag item, frame_type.
    SameFrame,
    /// 63-127
    /// This frame type indicates that the frame has exactly the same local variables as the
    /// previous frame and that the operand stack has one entry. The offset_delta value for the
    /// frame is given by the formula frame_type - 64. The verification type of the one stack entry
    /// appears after the frame type.
    SameLocals1StackItemFrame { offset_delta: u16, stack: VerificationTypeInfo },
    // 128-246 reserved
    /// 247
    /// This frame type indicates that the frame has exactly the same local variables as the
    /// previous frame and that the operand stack has one entry. The offset_delta value for the
    /// frame is given explicitly, unlike in the frame type same_locals_1_stack_item_frame. The
    /// verification type of the one stack entry appears after offset_delta.
    SameLocals1StackItemFrameExtended { offset_delta: u16, stack: VerificationTypeInfo },
    /// 248-250
    /// This frame type indicates that the frame has the same local variables as the previous
    /// frame except that the last k local variables are absent, and that the operand stack is
    /// empty. The value of k is given by the formula 251 - frame_type. The offset_delta value for
    /// the frame is given explicitly.
    ChopFrame { offset_delta: u16 },
    /// 251
    /// This frame type indicates that the frame has exactly the same local variables as the
    /// previous frame and that the operand stack is empty. The offset_delta value for the frame
    /// is given explicitly, unlike in the frame type same_frame.
    SameFrameExtended { offset_delta: u16 },
    /// 252-254
    /// This frame type indicates that the frame has the same locals as the previous frame
    /// except that k additional locals are defined, and that the operand stack is empty. The
    /// value of k is given by the formula frame_type - 251. The offset_delta value for the frame
    /// is given explicitly.
    AppendFrame { offset_delta: u16, locals: Vec<VerificationTypeInfo> },
    /// 256
    /// A full stack frame.
    FullFrame {
        offset_delta: u16,
        locals: Vec<VerificationTypeInfo>,
        stack: Vec<VerificationTypeInfo>,
    },
}
