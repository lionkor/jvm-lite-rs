use crate::access_flag::AccessFlag;
use crate::attribute_info::{
    AttributeInfo, CodeAttribute, LineNumberEntry, LineNumberTableAttribute, StackMapTableAttribute,
};
use crate::byte_span::ByteSpan;
use crate::constant_pool::ConstantPool;
use crate::constant_tag::ConstantTag;
use crate::cp_info::CpInfo;
use crate::exception_table::ExceptionTable;
use crate::field_info::FieldInfo;
use crate::method_info::MethodInfo;
use crate::stack_map_frame::{StackMapFrame, VerificationTypeInfo};
use log::{debug, info, trace};
use std::collections::HashMap;
use std::ops::{BitAnd, Not};
use strum::IntoEnumIterator;
use thiserror::Error;

/// An error during parsing of a JVM class file. Expects Java SE 22 class files, but does not
/// perform a version check. The error types themselves are self-explanatory.
#[derive(Error, Debug)]
pub enum ClassfileError {
    #[error("Java classfile magic 0xcafebabe not found at the beginning of the file: {0}")]
    InvalidMagic(anyhow::Error),
    #[error("Failed to read the classfile version: {0}")]
    InvalidVersion(anyhow::Error),
    #[error("Failed to read the constant pool size: {0}")]
    InvalidConstantPoolSize(anyhow::Error),
    #[error("Failed to read a constant pool entry, even though one was expected: {0}")]
    InvalidCpInfo(anyhow::Error),
    #[error(
        "Failed to read constant pool entry, because the provided constant tag is invalid: {0}"
    )]
    InvalidConstantTag(u8),
    #[error("Failed to read access flag, because number not a valid access flag: {0}")]
    InvalidAccessFlag(u16),
    #[error("Failed to read access flag: {0}")]
    NoAccessFlag(anyhow::Error),
    #[error("Failed to read string from constant pool, index {0}")]
    FailedStringGet(usize),
    #[error("The field '{0}' does not exist")]
    FieldDoesNotExist(String),
    #[error("The method '{0}' does not exist")]
    MethodDoesNotExist(String),
    #[error("The stack map frame type {0} is not valid")]
    UnknownStackMapFrameType(u8),
    #[error("The verification type info {0} is not valid")]
    UnknownVerificationTypeInfo(u8),
}

/// Parses a Java class file version, which consists of two 16-bit numbers. We silently expect this
/// to be between (45, 0) and (66, 0) but do not check since we're not sure if other versions would
/// also "just work". A check could be added here later, with a custom error type for the case
/// where it does not match.
fn consume_version(data: &mut ByteSpan) -> anyhow::Result<(u16, u16)> {
    Ok((data.consume_integer_u16()?, data.consume_integer_u16()?))
}

/// Consumes the Java class file magic bytes 0xcafebabe.
fn consume_magic(data: &mut ByteSpan) -> Result<(), ClassfileError> {
    match data.consume(&[0xca, 0xfe, 0xba, 0xbe]) {
        Ok(_) => Ok(()),
        Err(err) => Err(ClassfileError::InvalidMagic(err.into())),
    }
}

/// Consumes a string with a u16 length prefix. Expects Utf8.
fn consume_string(data: &mut ByteSpan) -> anyhow::Result<String> {
    let len: u16 = data.consume_integer_u16()?;
    Ok(String::from_utf8(data.consume_n(len as usize)?.to_vec())?)
}

/// Consumes a constant pool info (cpinfo) structure.
fn consume_cpinfo(data: &mut ByteSpan) -> anyhow::Result<CpInfo> {
    let tag = data.consume_integer_u8()?;
    let cpi;
    match ConstantTag::from_repr(tag).unwrap_or(ConstantTag::Unknown) {
        ConstantTag::Class => {
            cpi = CpInfo::ClassInfo {
                name_index: data.consume_integer_u16()?,
            }
        }
        ConstantTag::FieldRef => {
            cpi = CpInfo::FieldRef {
                name_index: data.consume_integer_u16()?,
                name_type_index: data.consume_integer_u16()?,
            }
        }
        ConstantTag::MethodRef => {
            cpi = CpInfo::MethodRef {
                name_index: data.consume_integer_u16()?,
                name_type_index: data.consume_integer_u16()?,
            }
        }
        ConstantTag::InterfaceMethodRef => {
            cpi = CpInfo::InterfaceMethodRef {
                name_index: data.consume_integer_u16()?,
                name_type_index: data.consume_integer_u16()?,
            }
        }
        ConstantTag::String => {
            cpi = CpInfo::StringInfo {
                string_index: data.consume_integer_u16()?,
            }
        }
        ConstantTag::Integer => cpi = CpInfo::IntegerInfo(data.consume_integer_u32()?),
        ConstantTag::Float => {
            let float_bytes: [u8; 4] = [0; 4];
            data.consume(&float_bytes[..])?;
            cpi = CpInfo::FloatInfo(float_bytes)
        }
        ConstantTag::Long => cpi = CpInfo::LongInfo(data.consume_integer_u64()?),
        ConstantTag::Double => {
            let float_bytes: [u8; 8] = [0; 8];
            data.consume(&float_bytes[..])?;
            cpi = CpInfo::DoubleInfo(float_bytes)
        }
        ConstantTag::NameAndType => {
            cpi = CpInfo::NameTypeInfo {
                name_index: data.consume_integer_u16()?,
                descriptor_index: data.consume_integer_u16()?,
            }
        }
        ConstantTag::Utf8 => cpi = CpInfo::Utf8Info(consume_string(data)?),
        ConstantTag::MethodHandle => {
            todo!()
        }
        ConstantTag::MethodType => {
            todo!()
        }
        ConstantTag::Dynamic => {
            todo!()
        }
        ConstantTag::InvokeDynamic => {
            todo!()
        }
        ConstantTag::Module => {
            todo!()
        }
        ConstantTag::Package => {
            todo!()
        }
        _ => {
            return Err(ClassfileError::InvalidConstantTag(tag).into());
        }
    }
    Ok(cpi)
}

/// Recursively consumes the constant pool and all its entries.
fn consume_constant_pool(data: &mut ByteSpan) -> anyhow::Result<ConstantPool> {
    let pool_size = match data.consume_integer_u16() {
        Ok(val) => val - 1, // 1-based index
        Err(err) => {
            return Err(ClassfileError::InvalidConstantPoolSize(err.into()).into());
        }
    };
    debug!("Constant pool size: {}", pool_size);
    let mut res = ConstantPool::new(pool_size as usize);

    // for historical reasons some entries are marked "extended". This has the consequence that the
    // previous one was larger, we just parse them all with the correct size and simply skip these
    // extended segments for that reason.
    let mut extend_next = false;
    // parse exactly as many CpInfo's as expected
    for i in 0..pool_size {
        if extend_next {
            extend_next = false;
            res.entries.push(CpInfo::EntryExtension);
            debug!(
                "Constant pool entry no. {} is an extension of {} ({:?})",
                i + 1,
                i,
                CpInfo::EntryExtension
            );
            continue;
        }
        debug!("Reading constant pool entry no. {}", i + 1);
        let entry = match consume_cpinfo(data) {
            Ok(val) => val,
            Err(err) => {
                return Err(ClassfileError::InvalidCpInfo(err).into());
            }
        };
        trace!("Constant pool entry no. {}: {:?}", i + 1, &entry);
        // as noted before, extended entries are just entries with more bytes (so, long and
        // double).
        extend_next = matches!(entry, CpInfo::LongInfo(_) | CpInfo::DoubleInfo(_));
        res.entries.push(entry);
    }

    Ok(res)
}

/// Consumes access flags from their binary format. Since they are stored in AND-ed form, they we
/// parse them into an array. This makes it easier to check later if a flag exists, since this is
/// not a particularly hot piece of code we don't really care that this is less efficient.
fn consume_access_flags(data: &mut ByteSpan) -> anyhow::Result<Vec<AccessFlag>> {
    let mut flag_raw = match data.consume_integer_u16() {
        Ok(val) => val,
        Err(err) => {
            return Err(ClassfileError::NoAccessFlag(err.into()).into());
        }
    };

    let mut flags = Vec::<AccessFlag>::new();

    for flag in AccessFlag::iter() {
        let raw = flag as u16;
        if flag_raw.bitand(raw) != 0 {
            flags.push(flag);
            flag_raw = flag_raw.bitand(raw.not());
        }
    }

    Ok(flags)
    // All bits of the access_flags item not assigned in Table 4.1-B are reserved for future use.
    // They should be set to zero in generated class files and should be ignored by Java Virtual
    // Machine implementations.
    // So we ignore them.
    /*if flag_raw != 0 {
        Err(ClassfileError::InvalidAccessFlag(flag_raw).into())
    } else {
        Ok(flags)
    }*/
}

/// Consumes interfaces as u16's.
fn consume_interfaces(data: &mut ByteSpan) -> anyhow::Result<Vec<u16>> {
    let count = data.consume_integer_u16()?;
    let mut vec = Vec::with_capacity(count as usize);
    for _ in 0..count {
        vec.push(data.consume_integer_u16()?);
    }
    Ok(vec)
}

/// Consumes a table of line number attributes.
fn consume_line_number_table_attribute(
    data: &mut ByteSpan,
    _pool: &ConstantPool,
) -> anyhow::Result<LineNumberTableAttribute> {
    let count = data.consume_integer_u16()?;
    let mut vec = Vec::with_capacity(count as usize);
    for _ in 0..count {
        vec.push(consume_line_number_entry(data)?);
    }
    Ok(LineNumberTableAttribute {
        line_number_table: vec,
    })
}

/// Consumes a verification type info structure.
fn consume_verification_type_info(
    data: &mut ByteSpan,
    _pool: &ConstantPool,
) -> anyhow::Result<VerificationTypeInfo> {
    let value = data.consume_integer_u8()?;
    let mut type_info = VerificationTypeInfo::from_repr(value)
        .ok_or::<ClassfileError>(ClassfileError::UnknownVerificationTypeInfo(value))?;
    match &mut type_info {
        VerificationTypeInfo::Object { cpool_index } => {
            *cpool_index = data.consume_integer_u16()?
        }
        VerificationTypeInfo::Uninitialized { offset } => *offset = data.consume_integer_u16()?,
        _ => { /* nothing to do */ }
    }
    Ok(type_info)
}

/// Consumes an array of verification type infos.
fn consume_verification_type_infos(
    data: &mut ByteSpan,
    pool: &ConstantPool,
    count: usize,
) -> anyhow::Result<Vec<VerificationTypeInfo>> {
    let mut vec = Vec::with_capacity(count);
    for _ in 0..count {
        vec.push(consume_verification_type_info(data, pool)?);
    }
    Ok(vec)
}

/// Consumes a stack map frame.
fn consume_stack_map_frame(
    data: &mut ByteSpan,
    pool: &ConstantPool,
) -> anyhow::Result<StackMapFrame> {
    let frame_type = data.consume_integer_u8()?;
    // these values are taken from the Java SE 22 virtual machine specification, they are just
    // hardcoded for specific types of frame. We don't actually care which frame type anything is,
    // but we parse it in case we care later.
    // we also just have to parse it so that we can then parse whatever comes later - we can't skip
    // these kinds of structures because they are variably sized.
    match frame_type {
        0..=63 => Ok(StackMapFrame::SameFrame),
        64..=127 => Ok(StackMapFrame::SameLocals1StackItemFrame {
            offset_delta: (frame_type - 64) as u16,
            stack: consume_verification_type_info(data, pool)?,
        }),
        247 => Ok(StackMapFrame::SameLocals1StackItemFrameExtended {
            offset_delta: data.consume_integer_u16()?,
            stack: consume_verification_type_info(data, pool)?,
        }),
        248..=250 => Ok(StackMapFrame::ChopFrame {
            offset_delta: data.consume_integer_u16()?,
        }),
        251 => Ok(StackMapFrame::SameFrameExtended {
            offset_delta: data.consume_integer_u16()?,
        }),
        252..=254 => Ok(StackMapFrame::AppendFrame {
            offset_delta: data.consume_integer_u16()?,
            locals: consume_verification_type_infos(data, pool, (frame_type - 251) as usize)?,
        }),
        255 => {
            let offset = data.consume_integer_u16()?;
            let locals_count = data.consume_integer_u16()? as usize;
            let locals = consume_verification_type_infos(data, pool, locals_count)?;
            let stack_size = data.consume_integer_u16()? as usize;
            let stack = consume_verification_type_infos(data, pool, stack_size)?;
            Ok(StackMapFrame::FullFrame {
                offset_delta: offset,
                locals,
                stack,
            })
        }
        _ => Err(ClassfileError::UnknownStackMapFrameType(frame_type).into()),
    }
}

/// Consumes an attribute of the stack map table type.
fn consume_stack_map_table_attribute(
    data: &mut ByteSpan,
    pool: &ConstantPool,
) -> anyhow::Result<StackMapTableAttribute> {
    let count = data.consume_integer_u16()?;
    let mut vec = Vec::with_capacity(count as usize);
    for _ in 0..count {
        vec.push(consume_stack_map_frame(data, pool)?);
    }
    Ok(StackMapTableAttribute { entries: vec })
}

/// Consumes a line number entry.
fn consume_line_number_entry(data: &mut ByteSpan) -> anyhow::Result<LineNumberEntry> {
    Ok(LineNumberEntry {
        start_pc: data.consume_integer_u16()?,
        line_number: data.consume_integer_u16()?,
    })
}

/// Consumes an attribute info, depending on the type name of the attribute.
fn consume_attribute(data: &mut ByteSpan, pool: &ConstantPool) -> anyhow::Result<AttributeInfo> {
    let name = pool
        .get_string(data.consume_integer_u16()? as usize)?
        .clone();
    let _info_size = data.consume_integer_u32()? as usize;
    match name.as_str() {
        // === method attributes ===
        "Code" => Ok(AttributeInfo::Code(consume_code_attribute(data, pool)?)),
        "LineNumberTable" => Ok(AttributeInfo::LineNumberTable(
            consume_line_number_table_attribute(data, pool)?,
        )),
        "StackMapTable" => Ok(AttributeInfo::StackMapTable(
            consume_stack_map_table_attribute(data, pool)?,
        )),
        // === field attributes ===
        "ConstantValue" => Ok(AttributeInfo::ConstantValue(
            pool.get(data.consume_integer_u16()? as usize)?.clone(),
        )),
        // === ===
        _ => {
            todo!("Attribute '{}'", name);
        }
    }
}

/// Consumes an exception table. These are not used but need to be parsed anyway.
fn consume_exception_table(data: &mut ByteSpan) -> anyhow::Result<ExceptionTable> {
    Ok(ExceptionTable {
        start_pc: data.consume_integer_u16()?,
        end_pc: data.consume_integer_u16()?,
        handler_pc: data.consume_integer_u16()?,
        catch_type: data.consume_integer_u16()?,
    })
}

fn consume_exception_tables(data: &mut ByteSpan) -> anyhow::Result<Vec<ExceptionTable>> {
    let count = data.consume_integer_u16()?;
    let mut vec = Vec::with_capacity(count as usize);
    for _ in 0..count {
        vec.push(consume_exception_table(data)?);
    }
    Ok(vec)
}

/// Consumes an array of exception tables. These are not used but need to be parsed anyway.
fn consume_code_attribute(
    data: &mut ByteSpan,
    pool: &ConstantPool,
) -> anyhow::Result<CodeAttribute> {
    let max_stack = data.consume_integer_u16()?;
    let max_locals = data.consume_integer_u16()?;
    let code_len = data.consume_integer_u32()?;
    let code = data.consume_n(code_len as usize)?.to_vec();
    let exception_tables = consume_exception_tables(data)?;
    let attributes = consume_attributes(data, pool)?;

    Ok(CodeAttribute {
        max_stack,
        max_locals,
        code,
        exception_tables,
        attributes,
    })
}

/// Consumes an array of attributes. A lot of things have this attached, as most things (classes,
/// fields, methods, etc.) have attributes.
fn consume_attributes(
    data: &mut ByteSpan,
    pool: &ConstantPool,
) -> anyhow::Result<Vec<AttributeInfo>> {
    let count = data.consume_integer_u16()?;
    let mut vec = Vec::with_capacity(count as usize);
    for _ in 0..count {
        vec.push(consume_attribute(data, pool)?);
    }
    Ok(vec)
}

/// Consumes a field info, which describes the name, type (descriptor), attributes, etc. of a
/// field. The name and type info are resolved, but not parsed at this stage.

fn consume_field_info(data: &mut ByteSpan, pool: &ConstantPool) -> anyhow::Result<FieldInfo> {
    Ok(FieldInfo {
        access_flags: consume_access_flags(data)?,
        name: pool
            .get_string(data.consume_integer_u16()? as usize)?
            .clone(),
        descriptor: pool
            .get_string(data.consume_integer_u16()? as usize)?
            .clone(),
        attributes: consume_attributes(data, pool)?,
    })
}

/// Consumes an array of fields.
fn consume_fields(data: &mut ByteSpan, pool: &ConstantPool) -> anyhow::Result<Vec<FieldInfo>> {
    let count = data.consume_integer_u16()?;
    let mut vec = Vec::with_capacity(count as usize);
    for _ in 0..count {
        let fieldinfo = consume_field_info(data, pool)?;
        vec.push(fieldinfo);
    }
    Ok(vec)
}

/// Consumes a method info and the associated type info, etc. The name and type info are resolved,
/// but not parsed at this stage.
fn consume_method_info(data: &mut ByteSpan, pool: &ConstantPool) -> anyhow::Result<MethodInfo> {
    Ok(MethodInfo {
        access_flags: consume_access_flags(data)?,
        name: pool
            .get_string(data.consume_integer_u16()? as usize)?
            .clone(),
        descriptor: pool
            .get_string(data.consume_integer_u16()? as usize)?
            .clone(),
        attributes: consume_attributes(data, pool)?,
    })
}

/// Consumes an array of methods.
fn consume_methods(data: &mut ByteSpan, pool: &ConstantPool) -> anyhow::Result<Vec<MethodInfo>> {
    let count = data.consume_integer_u16()?;
    let mut vec = Vec::with_capacity(count as usize);
    for _ in 0..count {
        vec.push(consume_method_info(data, pool)?);
    }
    Ok(vec)
}

/// Represents a Java `.class` file. Contains all data and metadata found in the class file.
#[derive(Debug)]
pub struct Classfile {
    pub minor: u16,
    pub major: u16,
    pub constant_pool: ConstantPool,
    pub access_flags: Vec<AccessFlag>,
    pub this_class: String,
    pub super_class: String,
    pub interfaces: Vec<u16>,
    fields: HashMap<String, FieldInfo>,
    methods: HashMap<String, MethodInfo>,
}

impl Classfile {
    /// Returns a field by field name, if it exists. The returned `FieldInfo` contains access
    /// flags, attributes, type information, etc.
    pub fn get_field(&self, name: &str) -> Result<&FieldInfo, ClassfileError> {
        self.fields
            .get(name)
            .ok_or(ClassfileError::FieldDoesNotExist(name.to_string()))
    }

    /// Whether a given field exists, by name.
    pub fn has_field(&self, name: &str) -> bool {
        self.fields.contains_key(name)
    }

    /// Returns a method by method name, if it exists. The returned `MethodInfo` contains access
    /// flags, attributes, type information, etc.
    pub fn get_method(&self, name: &str) -> Result<&MethodInfo, ClassfileError> {
        self.methods
            .get(name)
            .ok_or(ClassfileError::MethodDoesNotExist(name.to_string()))
    }

    /// Whether the given method exists, by name.
    pub fn has_method(&self, name: &str) -> bool {
        self.methods.contains_key(name)
    }

    /// Constructs a `Classfile` from bytes. This parses the entire class file, and returns it, and
    /// fails if any parsing step fails.
    pub fn from_data(raw_data: &Vec<u8>) -> anyhow::Result<Classfile> {
        let mut data = ByteSpan::new(raw_data.as_slice());

        consume_magic(&mut data)?;

        let (minor, major) = consume_version(&mut data)?;
        info!("Class file version {}.{}", major, minor);

        let constant_pool = consume_constant_pool(&mut data)?;
        info!("Constant pool:\n{:#?}", constant_pool);

        let access_flags = consume_access_flags(&mut data)?;
        info!("Access flag(s): {:?}", access_flags);

        let this_class = constant_pool
            .get_string(data.consume_integer_u16()? as usize)?
            .clone();
        info!("This class: {:?}", this_class);

        let super_class = constant_pool
            .get_string(data.consume_integer_u16()? as usize)?
            .clone();
        info!("Super class: {:?}", super_class);

        let interfaces = consume_interfaces(&mut data)?;
        info!("Interfaces: {:?}", interfaces);

        let fields = consume_fields(&mut data, &constant_pool)?;
        info!("Fields:\n{:#?}", fields);

        let fields_map: HashMap<String, FieldInfo> = fields
            .iter()
            .map(move |info| (info.name.clone(), info.clone()))
            .collect();

        let methods = consume_methods(&mut data, &constant_pool)?;
        info!("Methods:\n{:#?}", methods);

        let methods_map: HashMap<String, MethodInfo> = methods
            .iter()
            .map(move |info| (info.name.clone(), info.clone()))
            .collect();

        Ok(Classfile {
            minor,
            major,
            constant_pool,
            access_flags,
            this_class: this_class.clone(),
            super_class: super_class.clone(),
            interfaces,
            fields: fields_map,
            methods: methods_map,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::byte_span::ByteSpan;

    use super::*;

    #[test]
    fn test_consume_version() {
        let mut span = ByteSpan::new(&[1, 2, 3, 4]);
        let res = consume_version(&mut span);
        assert!(res.is_ok());
        let res = res.unwrap();
        assert_eq!(res.0, ((1 << 8) | 2) as u16);
        assert_eq!(res.1, ((3 << 8) | 4) as u16);
        assert!(span.region.is_empty());
    }

    #[test]
    fn test_consume_panic() {
        let mut span = ByteSpan::new(&[]);
        assert!(span.consume_n(1).is_err());
    }

    #[test]
    fn test_consume_string() {
        let mut span = ByteSpan::new(&[0x00, 0x05, 0x28, 0x49, 0x49, 0x29, 0x49]);
        let str = consume_string(&mut span);
        assert!(str.is_ok());
        assert_eq!(str.unwrap(), "(II)I");
        assert!(span.region.is_empty());
    }

    #[test]
    fn test_consume_integer() {
        let int = u8::MAX.to_be_bytes();
        let mut span = ByteSpan::new(&int);
        assert_eq!(span.consume_integer_u8().unwrap(), u8::MAX);
        assert!(span.region.is_empty());
        let int = u16::MAX.to_be_bytes();
        let mut span = ByteSpan::new(&int);
        assert_eq!(span.consume_integer_u16().unwrap(), u16::MAX);
        assert!(span.region.is_empty());
        let int = u32::MAX.to_be_bytes();
        let mut span = ByteSpan::new(&int);
        assert_eq!(span.consume_integer_u32().unwrap(), u32::MAX);
        assert!(span.region.is_empty());
        let int = u64::MAX.to_be_bytes();
        let mut span = ByteSpan::new(&int);
        assert_eq!(span.consume_integer_u64().unwrap(), u64::MAX);
        assert!(span.region.is_empty());
        let int = i8::MAX.to_be_bytes();
        let mut span = ByteSpan::new(&int);
        assert_eq!(span.consume_integer_i8().unwrap(), i8::MAX);
        assert!(span.region.is_empty());
        let int = i16::MAX.to_be_bytes();
        let mut span = ByteSpan::new(&int);
        assert_eq!(span.consume_integer_i16().unwrap(), i16::MAX);
        assert!(span.region.is_empty());
        let int = i8::MIN.to_be_bytes();
        let mut span = ByteSpan::new(&int);
        assert_eq!(span.consume_integer_i8().unwrap(), i8::MIN);
        assert!(span.region.is_empty());
        let int = i16::MIN.to_be_bytes();
        let mut span = ByteSpan::new(&int);
        assert_eq!(span.consume_integer_i16().unwrap(), i16::MIN);
        assert!(span.region.is_empty());
    }
}
