use std::mem::size_of;
use thiserror::Error;

/// An error in the byte span implementation, such as failure to consume
/// from a byte span.
#[derive(Error, Debug)]
pub enum ByteSpanError {
    #[error("The span is too short to consume the given bytes")]
    ConsumeLengthFailure,
    #[error("Failed to match the specified bytes while consuming span")]
    ConsumeMatchFailure,
    #[error("Failed to parse integer of size {0}")]
    InvalidInteger(usize),
}

/// A reference to a span of bytes, offers consume() pattern of functions for consuming primitive
/// datatypes. Consuming means that the region shrinks and the offset increases.
pub struct ByteSpan<'a> {
    pub region: &'a [u8],
    pub offset: usize,
}

impl<'a> ByteSpan<'a> {
    /// Creates a new ByteSpan from the full given slice.
    pub fn new<'b>(data: &'b [u8]) -> ByteSpan<'b> {
        ByteSpan::<'b> {
            region: data,
            offset: 0,
        }
    }

    /// Consumes the given sequence of bytes if the region starts with those
    /// bytes, otherwise fails.
    pub fn consume(&mut self, bytes: &[u8]) -> Result<(), ByteSpanError> {
        if self.region.len() < bytes.len() {
            Err(ByteSpanError::ConsumeLengthFailure)
        } else if self.region.starts_with(bytes) {
            self.region = &self.region[bytes.len()..];
            self.offset += bytes.len();
            Ok(())
        } else {
            Err(ByteSpanError::ConsumeMatchFailure)
        }
    }

    /// Consumes the given amount of bytes and returns them. Fails if the
    /// span region doesn't hold enough bytes.
    pub fn consume_n(&mut self, byte_count: usize) -> Result<&[u8], ByteSpanError> {
        if self.region.len() < byte_count {
            Err(ByteSpanError::ConsumeLengthFailure)
        } else {
            let res = &self.region[..byte_count];
            self.offset += byte_count;
            self.region = &self.region[byte_count..];
            Ok(res)
        }
    }

    /// Consumes a single unsigned byte, fails if there are not enough bytes left.
    pub fn consume_integer_u8(&mut self) -> Result<u8, ByteSpanError> {
        if self.region.is_empty() {
            Err(ByteSpanError::InvalidInteger(size_of::<u8>()))
        } else {
            let res = self.region[0];
            self.region = &self.region[1..];
            self.offset += 1;
            Ok(res)
        }
    }

    /// Consumes a single signed byte, fails if there are not enough bytes left.
    pub fn consume_integer_i8(&mut self) -> Result<i8, ByteSpanError> {
        if self.region.is_empty() {
            Err(ByteSpanError::InvalidInteger(size_of::<i8>()))
        } else {
            let res = self.region[0] as i8;
            self.region = &self.region[1..];
            self.offset += 1;
            Ok(res)
        }
    }

    /// Consumes an unsigned big-endian 16-bit integer, fails if there are not enough bytes left.
    pub fn consume_integer_u16(&mut self) -> Result<u16, ByteSpanError> {
        if (self.region).len() < size_of::<u16>() {
            Err(ByteSpanError::InvalidInteger(size_of::<u16>()))
        } else {
            let res = u16::from_be_bytes([self.region[0], self.region[1]]);
            self.region = &self.region[2..];
            self.offset += size_of::<u16>();
            Ok(res)
        }
    }

    /// Consumes a signed big-endian 16-bit integer, fails if there are not enough bytes left.
    pub fn consume_integer_i16(&mut self) -> Result<i16, ByteSpanError> {
        if (self.region).len() < size_of::<i16>() {
            Err(ByteSpanError::InvalidInteger(size_of::<i16>()))
        } else {
            let res = i16::from_be_bytes([self.region[0], self.region[1]]);
            self.region = &self.region[2..];
            self.offset += size_of::<i16>();
            Ok(res)
        }
    }

    /// Consumes an unsigned big-endian 32-bit integer, fails if there are not enough bytes left.
    pub fn consume_integer_u32(&mut self) -> Result<u32, ByteSpanError> {
        if (self.region).len() < size_of::<u32>() {
            Err(ByteSpanError::InvalidInteger(size_of::<u32>()))
        } else {
            let res = u32::from_be_bytes([
                self.region[0],
                self.region[1],
                self.region[2],
                self.region[3],
            ]);
            self.region = &self.region[4..];
            self.offset += size_of::<u32>();
            Ok(res)
        }
    }

    /// Consumes an unsigned big-endian 64-bit integer, fails if there are not enough bytes left.
    pub fn consume_integer_u64(&mut self) -> Result<u64, ByteSpanError> {
        if (self.region).len() < size_of::<u64>() {
            Err(ByteSpanError::InvalidInteger(size_of::<u64>()))
        } else {
            let res = u64::from_be_bytes([
                self.region[0],
                self.region[1],
                self.region[2],
                self.region[3],
                self.region[4],
                self.region[5],
                self.region[6],
                self.region[7],
            ]);
            self.region = &self.region[8..];
            self.offset += size_of::<u64>();
            Ok(res)
        }
    }
}
