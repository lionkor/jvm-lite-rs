use std::collections::{HashMap, VecDeque};
use std::fmt::{Debug, Display, Formatter};

use log::{debug, error, info, warn};
use strum::FromRepr;
use thiserror::Error;

use crate::access_flag::AccessFlag;
use crate::byte_span::ByteSpan;
use crate::classfile::Classfile;
use crate::cp_info::CpInfo;
use crate::java;
use crate::object::Ref;

/// A type of comparison, used in the implementation of comparison instructions to make the code
/// more generic.
#[derive(Debug)]
pub enum Cmp {
    EQ,
    NE,
    LT,
    LE,
    GT,
    GE,
}

/// Represents a JVM bytecode instruction with its associated binary bytecode value.
#[allow(non_camel_case_types)]
#[derive(FromRepr, Debug)]
#[repr(u8)]
pub enum Op {
    nop = 0x0,
    aconst_null = 0x1,
    iconst_m1 = 0x2,
    iconst_0 = 0x3,
    iconst_1 = 0x4,
    iconst_2 = 0x5,
    iconst_3 = 0x6,
    iconst_4 = 0x7,
    iconst_5 = 0x8,
    lconst_0 = 0x9,
    lconst_1 = 0xa,
    bipush = 0x10,
    sipush = 0x11,
    ldc = 0x12,
    ldc_w = 0x13,
    ldc2_w = 0x14,
    iload = 0x15,
    iload_0 = 0x1a,
    iload_1 = 0x1b,
    iload_2 = 0x1c,
    iload_3 = 0x1d,
    aload_0 = 0x2a,
    aload_1 = 0x2b,
    aload_2 = 0x2c,
    aload_3 = 0x2d,
    istore = 0x36,
    istore_0 = 0x3b,
    istore_1 = 0x3c,
    istore_2 = 0x3d,
    istore_3 = 0x3e,
    astore_0 = 0x4b,
    astore_1 = 0x4c,
    astore_2 = 0x4d,
    astore_3 = 0x4e,
    iadd = 0x60,
    isub = 0x64,
    imul = 0x68,
    idiv = 0x6c,
    irem = 0x70,
    iinc = 0x84,
    ifeq = 0x99,
    ifne = 0x9a,
    iflt = 0x9b,
    ifge = 0x9c,
    ifgt = 0x9d,
    ifle = 0x9e,
    if_icmpeq = 0x9f,
    if_icmpne = 0xa0,
    if_icmplt = 0xa1,
    if_icmpge = 0xa2,
    if_icmpgt = 0xa3,
    if_icmple = 0xa4,
    goto = 0xa7,
    ireturn = 0xac,
    r#return = 0xb1,
    getstatic = 0xb2,
    putstatic = 0xb3,
    putfield = 0xb5,
    invokevirtual = 0xb6,
    invokespecial = 0xb7,
    invokestatic = 0xb8,
}

/// An error which can occur during execution of code in the JVM.
#[derive(Debug, Error)]
pub enum VmError {
    // TODO: Does this need a class name as well?
    #[error("Couldn't find method {0}")]
    MethodNotFound(String),
    #[error("Couldn't find class {0}")]
    ClassNotFound(String),
    #[error("Expected reference at index {0}, instead got {1:#?}")]
    NotAReference(usize, CpInfo),
    #[error("Expected an element in the operand stack, but it's empty")]
    StackEmpty,
    #[error("Unknown error in {0}")]
    UnknownError(&'static str),
    #[error("Field not found (not resolved?): {0}")]
    FieldNotFound(ResolvedReference),
    #[error("Field type {0} is invalid")]
    FieldTypeInvalid(char),
    #[error("Invalid method descriptor found while resolving method: '{0}'")]
    InvalidMethodDescriptor(ResolvedReference),
    #[error("The given type descriptor is not valid")]
    InvalidDescriptor,
    #[error("The local variable at index {0} doesn't exist or hasn't been initialized")]
    InvalidLocal(usize),
    #[error("Expected object, instead got '{0:#?}'")]
    ExpectedObject(Operand),
    #[error("Type mismatch! Expected variant {0}, got {1} instead")]
    MismatchedTypes(Operand, Operand),
    #[error("Type mismatch! Expected integer")]
    ExpectedInt,
    #[error("Parameter count mismatch! {0}.{1} expected a different number of arguments")]
    ParameterCountMismatch(String, String),
}

/// An operand to an instruction, i.e. an argument to a bytecode instruction.
#[derive(Debug, Clone)]
pub enum Operand {
    Byte(u8),
    Char(char),
    Short(u16),
    Int(i32),
    Long(i64),
    Float(f32),
    Double(f64),
    Boolean(bool),
    ObjectRef { r#type: Option<Ref> },
}

impl Display for Operand {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Operand::Byte(_) => "byte",
            Operand::Char(_) => "char",
            Operand::Short(_) => "short",
            Operand::Int(_) => "int",
            Operand::Long(_) => "long",
            Operand::Float(_) => "float",
            Operand::Double(_) => "double",
            Operand::Boolean(_) => "boolean",
            Operand::ObjectRef { .. } => "Object",
        };
        write!(f, "{}", str)
    }
}

/// Represents a concrete, executable method, with resolved return type and argument types.
#[derive(Debug, Clone)]
pub struct Method {
    name: String,
    // None = `void` return type
    return_type: Option<Operand>,
    arguments: Vec<Operand>,
}

impl Display for Method {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(value) = &self.return_type {
            write!(f, "{}", value)?;
        } else {
            write!(f, "void")?;
        }
        write!(
            f,
            " {}({})",
            self.name.clone().replace('/', "."),
            self.arguments
                .iter()
                .map(|op| format!("{}", &op))
                .collect::<Vec<String>>()
                .join(", ")
        )
    }
}

/// A frame holds all local data needed during a method invocation.
/// Usually, it's constructed from the StackMapTable of the current Code attribute (of the method)
/// but for this implementation, right now, we don't care.
/// Instead, stack and locals are unbounded here.
/// TODO.
#[derive(Debug)]
pub struct Frame {
    locals: Vec<Operand>,
    op_stack: Vec<Operand>,
    last_pc: usize,
    /* constant pool is given by executing VM, not stored here */
}

/// A frame (stack frame) in the JVM. Contains local variables and the operand stack.
impl Frame {
    pub fn new() -> Self {
        Self {
            locals: Vec::new(),
            op_stack: Vec::new(),
            last_pc: 0,
        }
    }

    pub fn get_local(&self, i: usize) -> Option<&Operand> {
        self.locals.get(i)
    }

    pub fn set_local(&mut self, i: usize, op: Operand) {
        if i >= self.locals.len() {
            self.locals
                .resize(i + 1, Operand::ObjectRef { r#type: None })
        }
        self.locals[i] = op;
    }

    pub fn stack_push(&mut self, op: Operand) {
        self.op_stack.push(op);
    }

    pub fn stack_count(&self) -> usize {
        self.op_stack.len()
    }

    pub fn stack_pop(&mut self) -> Option<Operand> {
        self.op_stack.pop()
    }

    pub fn stack_pop_err(&mut self) -> Result<Operand, VmError> {
        self.op_stack.pop().ok_or(VmError::StackEmpty)
    }
}

pub struct VM {
    fields: HashMap<
        String, /* class name */
        HashMap<String /* field name */, Operand /* type + value */>,
    >,
    methods: HashMap<
        String, /* class name */
        HashMap<String /* method name */, Method /* method */>,
    >,
    frames: Vec<Frame>,
    excepted: bool,
}

#[derive(Clone)]
pub struct ResolvedReference {
    class_name: String,
    member_name: String,
    member_descriptor: String,
}

impl Display for ResolvedReference {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "<{}.{} : {}>",
            self.class_name, self.member_name, self.member_descriptor
        )
    }
}

impl Debug for ResolvedReference {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "<{}.{} : {}>",
            self.class_name, self.member_name, self.member_descriptor
        )
    }
}

#[derive(Debug)]
pub enum VmException {
    IncompatibleClassChangeError,
    NullPointerException,
    ArithmeticException,
}

impl VM {
    pub fn new() -> Self {
        Self {
            fields: HashMap::new(),
            methods: HashMap::new(),
            frames: vec![],
            excepted: false,
        }
    }

    /// "Flattens" the reference, such that all properties in the entire tree in the constant pool
    /// that this reference uses are accessible as struct fields in a ResolvedReference.
    fn flatten_reference(
        &mut self,
        classfile: &Classfile,
        arg: usize,
    ) -> anyhow::Result<ResolvedReference> {
        let resolved_arg = classfile.constant_pool.get(arg)?;
        let mut _name = None;
        let mut _name_type = None;
        match resolved_arg {
            CpInfo::InterfaceMethodRef {
                name_index,
                name_type_index,
            }
            | CpInfo::MethodRef {
                name_index,
                name_type_index,
            }
            | CpInfo::FieldRef {
                name_index,
                name_type_index,
            } => {
                _name = Some(classfile.constant_pool.get_string(*name_index as usize)?);
                _name_type = Some(classfile.constant_pool.get(*name_type_index as usize)?);
            }
            _ => {
                error!("{:#?}", resolved_arg);
                return Err(VmError::NotAReference(arg, resolved_arg.clone()).into());
            }
        }
        if let Some(name) = _name {
            if let Some(CpInfo::NameTypeInfo {
                name_index,
                descriptor_index,
            }) = _name_type
            {
                return Ok(ResolvedReference {
                    class_name: name.clone(),
                    member_name: classfile
                        .constant_pool
                        .get_string(*name_index as usize)?
                        .clone(),
                    member_descriptor: classfile
                        .constant_pool
                        .get_string(*descriptor_index as usize)?
                        .clone(),
                });
            }
        }
        Err(VmError::NotAReference(arg, resolved_arg.clone()).into())
    }

    fn exec_aload_n(&mut self, n: usize) -> anyhow::Result<()> {
        let local = self
            .current_frame()
            .get_local(n)
            .ok_or::<VmError>(VmError::StackEmpty)?
            .clone();
        self.current_frame_mut().stack_push(local.clone());
        info!("> aload_{} ({:?})", n, local);
        Ok(())
    }

    pub fn resolve_method(
        &mut self,
        classfile: &Classfile,
        method_idx: usize,
    ) -> anyhow::Result<()> {
        fn insert_default(
            map: &mut HashMap<String, Method>,
            reference: &ResolvedReference,
            method: Method,
        ) -> anyhow::Result<()> {
            if !map.contains_key(&reference.member_name) {
                map.insert(reference.member_name.clone(), method);
            }
            Ok(())
        }
        let reference = self.flatten_reference(classfile, method_idx)?;
        debug!("Resolving method {}", reference);

        let mut result = Method {
            name: format!("{}.{}", reference.class_name, reference.member_name),
            return_type: None,
            arguments: vec![],
        };

        // make a byte span to make expecting and consuming easier
        let des = reference.member_descriptor.clone();
        if !des.starts_with('(') {
            return Err(VmError::InvalidMethodDescriptor(reference.clone()).into());
        }
        // remove '('

        let param_des = des
            .split('(')
            .nth(1)
            .ok_or::<VmError>(VmError::InvalidMethodDescriptor(reference.clone()))?
            .split(')')
            .nth(0)
            .ok_or::<VmError>(VmError::InvalidMethodDescriptor(reference.clone()))?;

        let mut span = ByteSpan::new(param_des.as_bytes());

        loop {
            if span.region.is_empty() {
                break;
            }
            result
                .arguments
                .push(Self::resolve_field_descriptor(&mut span)?);
        }

        // resolve return type
        let ret = des
            .split(')')
            .nth(1)
            .ok_or::<VmError>(VmError::InvalidMethodDescriptor(reference.clone()))?;

        // not void means we need to assign something!
        if ret != "V" {
            let mut span = ByteSpan::new(ret.as_bytes());
            result.return_type = Some(Self::resolve_field_descriptor(&mut span)?);
        }

        debug!("Resolved method {:?}", result);
        if let Some(map) = self.methods.get_mut(&reference.class_name) {
            insert_default(map, &reference, result)?;
        } else {
            self.methods
                .insert(reference.class_name.clone(), HashMap::new());
            insert_default(
                self.methods.get_mut(&reference.class_name).unwrap(),
                &reference,
                result,
            )?;
        }
        Ok(())
    }

    pub fn resolve_field_descriptor(span: &mut ByteSpan) -> anyhow::Result<Operand> {
        fn helper(c: char) -> anyhow::Result<Operand> {
            match c {
                'B' => Ok(Operand::Byte(Default::default())),
                'C' => Ok(Operand::Char(Default::default())),
                'D' => Ok(Operand::Double(Default::default())),
                'F' => Ok(Operand::Float(Default::default())),
                'I' => Ok(Operand::Int(Default::default())),
                'J' => Ok(Operand::Long(Default::default())),
                'L' => {
                    // reference!
                    Ok(Operand::ObjectRef { r#type: None })
                }
                'S' => Ok(Operand::Short(Default::default())),
                'Z' => Ok(Operand::Boolean(Default::default())),
                '[' => {
                    todo!("Single dimensional array: https://docs.oracle.com/javase/specs/jvms/se19/html/jvms-4.html#jvms-4.3.2")
                }
                _ => Err(VmError::FieldTypeInvalid(c).into()),
            }
        }

        let mut desc = helper(span.consume_n(1)?[0] as char)?;
        if let Operand::ObjectRef { ref mut r#type } = &mut desc {
            // need to parse until ';'
            let end = span
                .region
                .iter()
                .position(|&x| x == b';')
                .ok_or::<VmError>(VmError::InvalidDescriptor)?;
            *r#type = Some(Ref::Type(
                String::from_utf8_lossy(span.consume_n(end)?).to_string(),
            ));
            // consume ;
            let _ = span.consume_n(1)?;
        }
        Ok(desc)
    }

    pub fn resolve_field(&mut self, classfile: &Classfile, field_idx: usize) -> anyhow::Result<()> {
        fn insert_default(
            map: &mut HashMap<String, Operand>,
            reference: &ResolvedReference,
        ) -> anyhow::Result<()> {
            if !map.contains_key(&reference.member_name) {
                // create kv
                let mut span = ByteSpan::new(reference.member_descriptor.as_bytes());
                map.insert(
                    reference.member_name.clone(),
                    VM::resolve_field_descriptor(&mut span)?,
                );
            }
            Ok(())
        }

        let reference = self.flatten_reference(classfile, field_idx)?;

        debug!("Resolving field {}", reference);

        if let Some(map) = self.fields.get_mut(&reference.class_name) {
            insert_default(map, &reference)?;
        } else {
            self.fields
                .insert(reference.class_name.clone(), HashMap::new());
            insert_default(
                self.fields.get_mut(&reference.class_name).unwrap(),
                &reference,
            )?;
        }
        Ok(())
    }

    fn set_field(
        &mut self,
        classfile: &Classfile,
        field_idx: usize,
        value: Operand,
    ) -> anyhow::Result<()> {
        let reference = self.flatten_reference(classfile, field_idx)?;
        let map = self
            .fields
            .get_mut(reference.class_name.as_str())
            .ok_or::<anyhow::Error>(VmError::FieldNotFound(reference.clone()).into())?;
        let found_value = map
            .get_mut(reference.member_name.as_str())
            .ok_or::<anyhow::Error>(VmError::FieldNotFound(reference.clone()).into())?;
        debug!("Assigning: {} = {:?}", reference.clone(), value);
        // TODO: Check type!
        *found_value = value;
        Ok(())
    }

    fn get_field(&mut self, classfile: &Classfile, field_idx: usize) -> anyhow::Result<Operand> {
        let reference = self.flatten_reference(classfile, field_idx)?;
        let map = self
            .fields
            .get(reference.class_name.as_str())
            .ok_or::<anyhow::Error>(VmError::FieldNotFound(reference.clone()).into())?;
        let found_value = map
            .get(reference.member_name.as_str())
            .ok_or::<anyhow::Error>(VmError::FieldNotFound(reference.clone()).into())?;
        // TODO: Check type!
        Ok(found_value.clone())
    }

    fn get_method(&mut self, class_name: &str, method_name: &str) -> anyhow::Result<Method> {
        let map = self
            .methods
            .get(class_name)
            .ok_or::<VmError>(VmError::ClassNotFound(class_name.to_string()))?;
        let found_value = map
            .get(method_name)
            .ok_or::<VmError>(VmError::MethodNotFound(method_name.to_string()))?;
        Ok(found_value.clone())
    }

    pub fn is_method_present(&self, classfile: &Classfile, method_name: &str) -> bool {
        classfile.has_method(method_name)
    }

    fn throw_exception(&mut self, ex: VmException, context: String) {
        error!("EXCEPTION: {:?} ({})", ex, context);
        self.excepted = true;
    }

    pub fn push_frame(&mut self) {
        debug!("--> Pushing new frame (#{})", self.frames.len());
        self.frames.push(Frame::new());
    }

    pub fn pop_frame(&mut self) {
        debug!("--> Popping frame {:?}", self.frames.last());
        self.frames
            .pop()
            .expect("Frame push and pop must be symmetric");
    }

    pub fn current_frame(&self) -> &Frame {
        self.frames.last().expect("There must always be a frame")
    }

    pub fn current_frame_mut(&mut self) -> &mut Frame {
        self.frames
            .last_mut()
            .expect("There must always be a frame")
    }

    pub fn move_pc<'d>(&mut self, code: &mut ByteSpan<'d>, raw_code: &'d [u8], offset: isize) {
        let offset: usize = ((self.current_frame().last_pc as isize) + offset) as usize;
        code.offset = offset;
        code.region = &raw_code[offset..];
    }

    pub fn run_method_in_classfile(
        &mut self,
        classfile: &Classfile,
        to_call: &str,
        args: Vec<Operand>,
    ) -> anyhow::Result<()> {
        info!("--> Run {}", to_call);

        let method_to_call = classfile.get_method(to_call)?;

        let code_attrib = method_to_call.get_code()?;
        let raw_code = &code_attrib.code;
        let mut code = ByteSpan::new(raw_code.as_slice());

        self.push_frame();

        for (i, arg) in args.into_iter().enumerate() {
            self.current_frame_mut().set_local(i, arg.clone());
        }

        // if it's not a static method, we must push the current object reference!
        if !method_to_call
            .access_flags
            .iter()
            .any(|&x| x == AccessFlag::Static)
        {
            // TODO: Make this a real reference to a real object!
            self.current_frame_mut().set_local(
                0,
                Operand::ObjectRef {
                    r#type: Some(Ref::This),
                },
            )
        }

        'interpret: while !code.region.is_empty() && !self.excepted {
            debug!("-> Frame: {:#?}", self.current_frame());
            // now save the pc for jumps later
            self.current_frame_mut().last_pc = code.offset;

            let val = code.region[0];
            code.region = &code.region[1..];
            code.offset += 1;

            let opcode = match Op::from_repr(val) {
                None => {
                    todo!("Encountered INVALID instruction {:#x}", val);
                }
                Some(opcode) => opcode,
            };
            match opcode {
                Op::getstatic => {
                    let arg = code.consume_integer_u16()? as usize;
                    self.resolve_field(classfile, arg)?;
                    let reference = self.flatten_reference(classfile, arg)?;
                    info!("> getstatic #{} {}", arg, reference);
                    let field = self.get_field(classfile, arg)?;
                    self.current_frame_mut().stack_push(field);
                }
                Op::putstatic => {
                    let arg = code.consume_integer_u16()? as usize;
                    self.resolve_field(classfile, arg)?;
                    let value = self.current_frame_mut().stack_pop_err()?;
                    let reference = self.flatten_reference(classfile, arg)?;
                    info!("> putstatic #{} {}", arg, reference);
                    self.set_field(classfile, arg, value)?;
                }
                Op::ldc => {
                    let arg = code.consume_integer_u8()?;
                    self.exec_ldc(classfile, arg as usize)?;
                    info!("> ldc #{}", arg);
                }
                Op::ldc_w => {
                    let arg = code.consume_integer_u16()?;
                    self.exec_ldc(classfile, arg as usize)?;
                    info!("> ldc_w #{}", arg);
                }
                Op::ldc2_w => {
                    let arg = code.consume_integer_u16()?;
                    self.exec_ldc(classfile, arg as usize)?;
                    info!("> ldc2_w #{}", arg);
                }
                Op::invokevirtual => {
                    let arg = code.consume_integer_u16()? as usize;
                    self.resolve_method(classfile, arg)?;
                    let reference = self.flatten_reference(classfile, arg)?;
                    info!("> invokevirtual #{} {}", arg, reference);
                    let mut method =
                        self.get_method(&reference.class_name, &reference.member_name)?;
                    let frame = self.current_frame_mut();
                    for argument in &mut method.arguments {
                        *argument = frame.stack_pop_err()?;
                    }
                    // TODO: Use object_reference
                    let _object_reference = frame.stack_pop_err()?;
                    debug!("---> Invoking `{}`", method);
                    if self.try_run_native(
                        &reference.class_name,
                        &reference.member_name,
                        &mut method,
                    )? {
                        debug!("---> Invoked `{}` as native code", method);
                    } else {
                        todo!()
                    }
                }
                Op::aconst_null => {
                    self.current_frame_mut()
                        .stack_push(Operand::ObjectRef { r#type: None });
                    info!("> {:?}", opcode);
                }
                Op::iconst_m1 => {
                    self.current_frame_mut().stack_push(Operand::Int(-1));
                    info!("> {:?}", opcode);
                }
                Op::iconst_0 => {
                    self.current_frame_mut().stack_push(Operand::Int(0));
                    info!("> {:?}", opcode);
                }
                Op::iconst_1 => {
                    self.current_frame_mut().stack_push(Operand::Int(1));
                    info!("> {:?}", opcode);
                }
                Op::iconst_2 => {
                    self.current_frame_mut().stack_push(Operand::Int(2));
                    info!("> {:?}", opcode);
                }
                Op::iconst_3 => {
                    self.current_frame_mut().stack_push(Operand::Int(3));
                    info!("> {:?}", opcode);
                }
                Op::iconst_4 => {
                    self.current_frame_mut().stack_push(Operand::Int(4));
                    info!("> {:?}", opcode);
                }
                Op::iconst_5 => {
                    self.current_frame_mut().stack_push(Operand::Int(5));
                    info!("> {:?}", opcode);
                }
                Op::lconst_0 => {
                    self.current_frame_mut().stack_push(Operand::Long(0));
                    info!("> {:?}", opcode);
                }
                Op::lconst_1 => {
                    self.current_frame_mut().stack_push(Operand::Long(1));
                    info!("> {:?}", opcode);
                }
                Op::iload => {
                    let n = code.consume_integer_u8()? as usize;
                    self.exec_iload_n(n)?;
                    info!("> {:?} {}", opcode, n);
                }
                Op::iload_0 => {
                    self.exec_iload_n(0)?;
                    info!("> {:?} {}", opcode, 0);
                }
                Op::iload_1 => {
                    self.exec_iload_n(1)?;
                    info!("> {:?} {}", opcode, 1);
                }
                Op::iload_2 => {
                    self.exec_iload_n(2)?;
                    info!("> {:?} {}", opcode, 2);
                }
                Op::iload_3 => {
                    self.exec_iload_n(3)?;
                    info!("> {:?} {}", opcode, 3);
                }
                Op::aload_0 => {
                    self.exec_aload_n(0)?;
                    info!("> {:?} {}", opcode, 0);
                }
                Op::aload_1 => {
                    self.exec_aload_n(1)?;
                    info!("> {:?} {}", opcode, 1);
                }
                Op::aload_2 => {
                    self.exec_aload_n(2)?;
                    info!("> {:?} {}", opcode, 2);
                }
                Op::aload_3 => {
                    self.exec_aload_n(3)?;
                    info!("> {:?} {}", opcode, 3);
                }
                Op::istore => {
                    let n = code.consume_integer_u8()? as usize;
                    self.exec_istore_n(n)?;
                    info!("> {:?} {}", opcode, n);
                }
                Op::istore_0 => {
                    self.exec_istore_n(0)?;
                    info!("> {:?} {}", opcode, 0);
                }
                Op::istore_1 => {
                    self.exec_istore_n(1)?;
                    info!("> {:?} {}", opcode, 1);
                }
                Op::istore_2 => {
                    self.exec_istore_n(2)?;
                    info!("> {:?} {}", opcode, 2);
                }
                Op::istore_3 => {
                    self.exec_istore_n(3)?;
                    info!("> {:?} {}", opcode, 3);
                }
                Op::iadd => {
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    if let Operand::Int(a) = frame.stack_pop().unwrap() {
                        if let Operand::Int(b) = frame.stack_pop().unwrap() {
                            info!("> {:?} {} + {}", opcode, a, b);
                            frame.stack_push(Operand::Int(a + b))
                        } else {
                            return Err(VmError::ExpectedInt.into());
                        }
                    } else {
                        return Err(VmError::ExpectedInt.into());
                    }
                }
                Op::isub => {
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    if let Operand::Int(a) = frame.stack_pop().unwrap() {
                        if let Operand::Int(b) = frame.stack_pop().unwrap() {
                            info!("> {:?} {} - {}", opcode, b, a);
                            frame.stack_push(Operand::Int(b - a))
                        } else {
                            return Err(VmError::ExpectedInt.into());
                        }
                    } else {
                        return Err(VmError::ExpectedInt.into());
                    }
                }
                Op::imul => {
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    if let Operand::Int(a) = frame.stack_pop().unwrap() {
                        if let Operand::Int(b) = frame.stack_pop().unwrap() {
                            info!("> {:?} {} * {}", opcode, a, b);
                            frame.stack_push(Operand::Int(a * b))
                        } else {
                            return Err(VmError::ExpectedInt.into());
                        }
                    } else {
                        return Err(VmError::ExpectedInt.into());
                    }
                }
                Op::idiv => {
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    if let Operand::Int(a) = frame.stack_pop().unwrap() {
                        if let Operand::Int(b) = frame.stack_pop().unwrap() {
                            if a == 0 {
                                self.throw_exception(
                                    VmException::ArithmeticException,
                                    format!("/ by zero: {} / 0", b),
                                );
                            } else {
                                info!("> {:?} {} / {}", opcode, b, a);
                                frame.stack_push(Operand::Int(b / a))
                            }
                        } else {
                            return Err(VmError::ExpectedInt.into());
                        }
                    } else {
                        return Err(VmError::ExpectedInt.into());
                    }
                }
                Op::irem => {
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    if let Operand::Int(a) = frame.stack_pop().unwrap() {
                        if let Operand::Int(b) = frame.stack_pop().unwrap() {
                            if a == 0 {
                                self.throw_exception(
                                    VmException::ArithmeticException,
                                    format!("% by zero: {} % 0", b),
                                );
                            } else {
                                info!("> {:?} {} % {}", opcode, b, a);
                                frame.stack_push(Operand::Int(b % a))
                            }
                        } else {
                            return Err(VmError::ExpectedInt.into());
                        }
                    } else {
                        return Err(VmError::ExpectedInt.into());
                    }
                }
                Op::iinc => {
                    let idx = code.consume_integer_u8()? as usize;
                    let constant = code.consume_integer_i8()? as i32;
                    let frame = self.current_frame_mut();
                    let int = match frame.get_local(idx).ok_or(VmError::StackEmpty)? {
                        Operand::Int(i) => i,
                        invalid => {
                            return Err(
                                VmError::MismatchedTypes(Operand::Int(0), invalid.clone()).into()
                            )
                        }
                    };
                    frame.set_local(idx, Operand::Int(int + constant));
                    info!("> {:?} #{} by +{}", opcode, idx, constant);
                }
                Op::ifeq => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    let lhs = frame.stack_pop_err()?;
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::EQ, Operand::Int(0), lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::ifne => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    let lhs = frame.stack_pop_err()?;
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::NE, Operand::Int(0), lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::iflt => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    let lhs = frame.stack_pop_err()?;
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::LT, Operand::Int(0), lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::ifge => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    let lhs = frame.stack_pop_err()?;
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::GE, Operand::Int(0), lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::ifgt => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    let lhs = frame.stack_pop_err()?;
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::GT, Operand::Int(0), lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::ifle => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    let lhs = frame.stack_pop_err()?;
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::LE, Operand::Int(0), lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::if_icmpeq => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    let rhs = frame.stack_pop().unwrap();
                    let lhs = frame.stack_pop().unwrap();
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::EQ, rhs, lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::if_icmpne => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    let rhs = frame.stack_pop().unwrap();
                    let lhs = frame.stack_pop().unwrap();
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::NE, rhs, lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::if_icmplt => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    let rhs = frame.stack_pop().unwrap();
                    let lhs = frame.stack_pop().unwrap();
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::LT, rhs, lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::if_icmpge => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    let rhs = frame.stack_pop().unwrap();
                    let lhs = frame.stack_pop().unwrap();
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::GE, rhs, lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::if_icmpgt => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    let rhs = frame.stack_pop().unwrap();
                    let lhs = frame.stack_pop().unwrap();
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::GT, rhs, lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::if_icmple => {
                    let offset = code.consume_integer_i16()?;
                    let frame = self.current_frame_mut();
                    if frame.stack_count() < 2 {
                        return Err(VmError::StackEmpty.into());
                    }
                    let rhs = frame.stack_pop().unwrap();
                    let lhs = frame.stack_pop().unwrap();
                    info!("> {:?}", opcode);
                    if self.exec_if_icmp(Cmp::LE, rhs, lhs)? {
                        info!("-> true, pc{:+}", offset);
                        self.move_pc(&mut code, raw_code, offset as isize);
                    }
                    info!("-> false");
                }
                Op::nop => {
                    warn!("> {:?}", opcode);
                }
                Op::goto => {
                    let offset = code.consume_integer_i16()? as isize;
                    self.move_pc(&mut code, raw_code, offset);
                    info!("> {:?} pc{:+}", opcode, offset);
                }
                Op::ireturn => {
                    let value = self.current_frame_mut().stack_pop_err()?;
                    info!("> {:?} {:?}", opcode, value);
                    self.pop_frame();
                    info!("--> {} done", to_call);
                    // TODO: Check return type. If the return type isn't void, this is an invalid operation.

                    self.current_frame_mut().stack_push(value);

                    break 'interpret;
                }
                Op::r#return => {
                    self.pop_frame();
                    info!("> {:?}", opcode);
                    info!("--> {} done", to_call);
                    // TODO: Check return type. If the return type isn't void, this is an invalid operation.
                    break 'interpret;
                }
                Op::putfield => {
                    self.exec_putfield(&mut code, classfile, to_call)?;
                }
                Op::invokespecial => {
                    self.exec_invokespecial(&mut code, classfile)?;
                }
                Op::invokestatic => {
                    self.exec_invokestatic(&mut code, classfile)?;
                }
                Op::bipush => {
                    let arg = code.consume_integer_u8()?;
                    self.current_frame_mut()
                        .stack_push(Operand::Int(arg as i32));
                    info!("> {:?} {}", opcode, arg);
                }
                Op::sipush => {
                    let arg = code.consume_integer_u16()?;
                    self.current_frame_mut()
                        .stack_push(Operand::Int(arg as i32));
                    info!("> {:?} {}", opcode, arg);
                }
                _ => {
                    todo!("{:?}", opcode);
                }
            }
        }
        Ok(())
    }

    fn exec_if_icmp(&mut self, cmp: Cmp, rhs: Operand, lhs: Operand) -> anyhow::Result<bool> {
        let rhs = match rhs {
            Operand::Int(value) => value,
            val => return Err(VmError::MismatchedTypes(Operand::Int(0), val).into()),
        };
        let lhs = match lhs {
            Operand::Int(value) => value,
            val => return Err(VmError::MismatchedTypes(Operand::Int(0), val).into()),
        };
        info!("-> {} {:?} {}", lhs, cmp, rhs);
        Ok(match cmp {
            Cmp::EQ => lhs == rhs,
            Cmp::NE => lhs != rhs,
            Cmp::LT => lhs < rhs,
            Cmp::LE => lhs <= rhs,
            Cmp::GT => lhs > rhs,
            Cmp::GE => lhs >= rhs,
        })
    }

    fn exec_putfield(
        &mut self,
        code: &mut ByteSpan,
        classfile: &Classfile,
        to_call: &str,
    ) -> anyhow::Result<()> {
        let arg = code.consume_integer_u16()? as usize;
        self.resolve_field(classfile, arg)?;
        let field_reference = self.flatten_reference(classfile, arg)?;
        let value = self.current_frame_mut().stack_pop_err()?;
        let objref = self.current_frame_mut().stack_pop_err()?;
        // TODO: Check type?
        let field = classfile.get_field(&field_reference.member_name)?;
        info!(
            "> putfield #{} (field: {}, ref: {:?}, value: {:?}, info: {:?})",
            arg, field_reference, objref, value, field
        );
        if field.has_access_flag(AccessFlag::Static) {
            self.throw_exception(
                VmException::IncompatibleClassChangeError,
                format!(
                    "In method {0}.{1} putfield: Field {0}.{2} is static",
                    classfile.this_class, to_call, field.name
                ),
            );
        }
        if let Operand::ObjectRef { r#type: None } = objref {
            if to_call != "<clinit>" && to_call != "<init>" {
                self.throw_exception(
                    VmException::NullPointerException,
                    format!(
                        "In method {0}.{1} putfield: Ref is null",
                        classfile.this_class, to_call
                    ),
                );
            } else {
                self.set_field(classfile, arg, value)?;
            }
        }
        Ok(())
    }

    fn exec_invokespecial(
        &mut self,
        code: &mut ByteSpan,
        classfile: &Classfile,
    ) -> anyhow::Result<()> {
        let arg = code.consume_integer_u16()? as usize;
        self.resolve_method(classfile, arg)?;
        let reference = self.flatten_reference(classfile, arg)?;
        let frame = self.current_frame_mut();
        let obj_ref = frame.stack_pop_err()?;
        if let Operand::ObjectRef {
            r#type: Some(value),
        } = &obj_ref
        {
            match value {
                Ref::Object => {}
                Ref::This => {}
                _ => return Err(VmError::ExpectedObject(obj_ref.clone()))?,
            }
        } else {
            return Err(VmError::ExpectedObject(obj_ref.clone()))?;
        }
        let mut method = self.get_method(&reference.class_name, &reference.member_name)?;
        debug!("---> Invoking `{}`", method);
        if self.try_run_native(&reference.class_name, &reference.member_name, &mut method)? {
            debug!("---> Invoked `{}` as native code", method);
        } else {
            todo!()
        }
        Ok(())
    }

    fn try_run_native(
        &mut self,
        class_: &str,
        name: &str,
        method_data: &mut Method,
    ) -> anyhow::Result<bool> {
        match class_ {
            "java/lang/Object" => match name {
                "<init>" => {
                    java::lang::object::ctor();
                    return Ok(true);
                }
                &_ => {
                    todo!("{}.{}", class_, name)
                }
            },
            "java/io/PrintStream" => {
                if name == "println" {
                    if method_data.arguments.len() != 1 {
                        return Err(VmError::ParameterCountMismatch(
                            class_.to_string(),
                            name.to_string(),
                        )
                        .into());
                    }
                    let arg = method_data.arguments.get(0).unwrap();
                    match arg {
                        Operand::ObjectRef { r#type } => {
                            match r#type {
                                None => {}
                                Some(value) => {
                                    return match value {
                                        Ref::String(str) => {
                                            // TODO: Check that the instance is stdout
                                            java::io::printstream::stdout_println(str.to_string());
                                            Ok(true)
                                        }
                                        _ => {
                                            let expected = Operand::ObjectRef {
                                                r#type: Some(Ref::String("".to_string())),
                                            };
                                            Err(VmError::MismatchedTypes(
                                                expected.clone(),
                                                arg.clone(),
                                            )
                                            .into())
                                        }
                                    };
                                }
                            }
                        }
                        Operand::Int(value) => {
                            java::io::printstream::stdout_println(format!("{}", value));
                            return Ok(true);
                        }
                        _ => {
                            return Err(
                                VmError::MismatchedTypes(Operand::Int(0), arg.clone()).into()
                            );
                        }
                    }
                }
            }
            &_ => {}
        }
        Ok(false)
    }
    fn exec_ldc(&mut self, classfile: &Classfile, idx: usize) -> anyhow::Result<()> {
        let value = classfile.constant_pool.get(idx)?;
        let op: Operand;
        match value.clone() {
            CpInfo::ClassInfo { .. } => {
                todo!()
            }
            CpInfo::FieldRef { .. } => {
                self.resolve_field(classfile, idx)?;
                let reference = self.flatten_reference(classfile, idx)?;
                op = Operand::ObjectRef {
                    r#type: Some(Ref::Field(reference)),
                }
            }
            CpInfo::MethodRef { .. } => {
                self.resolve_method(classfile, idx)?;
                let reference = self.flatten_reference(classfile, idx)?;
                op = Operand::ObjectRef {
                    r#type: Some(Ref::Method(reference)),
                }
            }
            CpInfo::InterfaceMethodRef { .. } => {
                // TODO: Valid?
                self.resolve_method(classfile, idx)?;
                let reference = self.flatten_reference(classfile, idx)?;
                op = Operand::ObjectRef {
                    r#type: Some(Ref::Method(reference)),
                }
            }
            CpInfo::StringInfo { .. } => {
                op = Operand::ObjectRef {
                    r#type: Some(Ref::String(
                        classfile.constant_pool.get_string(idx)?.clone(),
                    )),
                }
            }
            CpInfo::IntegerInfo(val) => {
                op = Operand::Int(val as i32);
            }
            CpInfo::FloatInfo(val) => {
                op = Operand::Float(f32::from_be_bytes(val));
            }
            CpInfo::LongInfo(val) => {
                op = Operand::Long(val as i64);
            }
            CpInfo::DoubleInfo(val) => {
                op = Operand::Double(f64::from_be_bytes(val));
            }
            CpInfo::NameTypeInfo { .. } => {
                todo!()
            }
            CpInfo::Utf8Info(str) => {
                op = Operand::ObjectRef {
                    r#type: Some(Ref::String(str)),
                };
            }
            CpInfo::EntryExtension => {
                unreachable!()
            }
        }
        self.current_frame_mut().stack_push(op);
        Ok(())
    }
    fn exec_istore_n(&mut self, local_var_idx: usize) -> anyhow::Result<()> {
        let frame = self.current_frame_mut();
        let stack_top = frame.stack_pop_err()?;
        frame.set_local(local_var_idx, stack_top);
        Ok(())
    }
    fn exec_iload_n(&mut self, local_var_idx: usize) -> anyhow::Result<()> {
        let frame = self.current_frame_mut();
        let value = frame
            .get_local(local_var_idx)
            .ok_or(VmError::InvalidLocal(local_var_idx))?;
        if let Operand::Int(int) = value {
            frame.stack_push(Operand::Int(*int));
            Ok(())
        } else {
            Err(VmError::MismatchedTypes(Operand::Int(0), value.clone()).into())
        }
    }
    fn exec_invokestatic(
        &mut self,
        code: &mut ByteSpan,
        classfile: &Classfile,
    ) -> anyhow::Result<()> {
        let arg = code.consume_integer_u16()? as usize;
        self.resolve_method(classfile, arg)?;
        let reference = self.flatten_reference(classfile, arg)?;
        let mut method = self.get_method(&reference.class_name, &reference.member_name)?;
        let frame = self.current_frame_mut();
        for argument in &mut method.arguments {
            *argument = frame.stack_pop_err()?;
        }
        debug!("--> Calling {}", method);

        let args: Vec<Operand> = method.arguments.iter().rev().cloned().collect();
        self.run_method_in_classfile(classfile, &reference.member_name, args)
    }
}
