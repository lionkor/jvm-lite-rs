use crate::access_flag::AccessFlag;
use crate::attribute_info::AttributeInfo;

/// Contains information about a field of a class, including type info and access flags. The type
/// name is not resolved, and must be parsed to be usable.
#[derive(Debug, Clone)]
pub struct FieldInfo {
    pub access_flags: Vec<AccessFlag>,
    pub name: String,
    pub descriptor: String,
    pub attributes: Vec<AttributeInfo>,
}

impl FieldInfo {
    /// Whether this field's access flags contain the given flag.
    pub fn has_access_flag(&self, access_flag: AccessFlag) -> bool {
        self.access_flags.iter().any(|flag| *flag == access_flag)
    }
}
