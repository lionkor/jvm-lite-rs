use strum::FromRepr;

/// A marker used in the constnat pool in order to tag which kind of data something is.
#[derive(FromRepr, Debug)]
#[repr(u8)]
pub enum ConstantTag {
    Class = 7,
    FieldRef = 9,
    MethodRef = 10,
    InterfaceMethodRef = 11,
    String = 8,
    Integer = 3,
    Float = 4,
    Long = 5,
    Double = 6,
    NameAndType = 12,
    Utf8 = 1,
    MethodHandle = 15,
    MethodType = 16,
    Dynamic = 17,
    InvokeDynamic = 18,
    Module = 19,
    Package = 20,

    Unknown = 0xff,
}
