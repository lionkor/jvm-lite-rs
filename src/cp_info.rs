use thiserror::Error;

#[derive(Error, Debug)]
pub enum CpInfoError {}

/// A node in the constant pool "tree" structure. This is not directly a tree, as in, there is no
/// nodes and leaves and so forth. Instead, most elements point towards an index in the same
/// constnat pool array they exist in. However, an entry may point to another entry, which in turn
/// points to one or more entries, etc. hence the reference to a "tree".
#[derive(Debug, Clone)]
pub enum CpInfo {
    ClassInfo {
        name_index: u16,
    },
    FieldRef {
        name_index: u16,
        name_type_index: u16,
    },
    MethodRef {
        name_index: u16,
        name_type_index: u16,
    },
    InterfaceMethodRef {
        name_index: u16,
        name_type_index: u16,
    },
    StringInfo {
        string_index: u16,
    },
    IntegerInfo(u32),
    FloatInfo([u8; 4]),
    // TODO: Parse into f32 for here?
    LongInfo(u64),
    DoubleInfo([u8; 8]),
    // TODO: Parse into f64 for here?
    NameTypeInfo {
        name_index: u16,
        descriptor_index: u16,
    },
    Utf8Info(String),
    // Used for long and double, takes up an entry but is empty
    EntryExtension,
}
