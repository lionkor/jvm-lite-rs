extern crate core;

use std::env;
use std::fs::File;
use std::io::Read;
use std::process::exit;

use log::error;

use crate::object::Ref;
use crate::vm::Operand;

mod access_flag;
mod attribute_info;
mod byte_span;
mod classfile;
mod constant_pool;
mod constant_tag;
mod cp_info;
mod exception_table;
mod field_info;
mod java;
mod method_info;
mod object;
mod stack_map_frame;
mod vm;

/// Executes the given classfile from the path, passes the arguments to it.
/// Assumes that the classfile contains a `main` function.
fn run_classfile(classfile_path: &String, args: &[String]) -> anyhow::Result<()> {
    let mut content = vec![];

    File::open(classfile_path)?.read_to_end(&mut content)?;

    // parse the classfile from the file, this may fail if the file is invalid
    let classfile = classfile::Classfile::from_data(&content)?;

    let mut vm = vm::VM::new();

    // clinit is the class initialization function, which will initialize class members.
    // it may not be present, but if it is, it has to run first.
    if vm.is_method_present(&classfile, "<clinit>") {
        vm.run_method_in_classfile(&classfile, "<clinit>", vec![])?;
    }
    // <init> has to exist and initializes all sorts of other fields.
    vm.run_method_in_classfile(&classfile, "<init>", vec![])?;
    // convert all string arguments into the `String[]` Java type
    let main_args: Vec<Operand> = args
        .iter()
        .map(|arg| Operand::ObjectRef {
            r#type: Some(Ref::String(arg.clone())),
        })
        .collect();
    // execute main, this returns when the program terminates for any reason, or errors
    // if an error occurs.
    vm.run_method_in_classfile(&classfile, "main", main_args)?;

    Ok(())
}

fn main() -> anyhow::Result<()> {
    // the given classfile is run in a thread so that we can decide how large we want
    // the stack to be. Since the rust stack is used as the VM's callstack as well,
    // we want this to be huge.
    let thread = std::thread::Builder::new()
        .stack_size(16 * 1024 * 1024) // 16 MiB stack
        .spawn(|| {
            env_logger::builder().parse_default_env().init();

            let args: Vec<_> = env::args().collect();

            if args.len() < 2 {
                error!("Invalid arguments, expected: <classfile>");
                exit(1);
            }

            if let Err(err) = run_classfile(&args[1], &args[1..]) {
                error!("Failed to parse and run classfile: {:?}", err);
                exit(1);
            }
        })?;
    let _ = thread.join();
    Ok(())
}
