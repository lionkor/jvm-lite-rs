# JVM Lite

## How to run

We assume that `Myclass.class` is the Java class file you want to run. You can find an example in the `java/` folder.

1. Install the Rust toolchain: https://www.rust-lang.org/learn/get-started.
2. Verify that the Rust toolchain is installed by running `cargo --version`, which should return a version.
3. In the root of this source tree, run the following command to build it:
```sh
cargo build
```
or to build the release (optimized) version:
```sh
cargo build --release
```
4. To run it, run the following command:
```sh
cargo run -- Myclass.class
```
or to run the release version:
```sh
cargo run --release -- Myclass.class
```

Alternatively, you can find executable versions in the `bin/` folder. The `jvm-lite.exe` is a Windows executable, and the `jvm-lite` is a statically linked Linux executable. Both are not release builds, so they are not optimized. This means that they contain logging code, which can be turned on via supplying the ENV variable `RUST_LOG`, for example `RUST_LOG=info` or `RUST_LOG=debug`. Some of these are disabled in release builds.

You can run these builds like so:

Linux:
```sh
./bin/jvm-lite Myclass.class
```

Windows Powershell:
```sh
.\bin\jvm-lite.exe Myclass.class
```
